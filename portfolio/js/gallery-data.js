var galleryData = [
	{
		"show": true,
		"title": 'Web Development Portfolio',
		"subtitle": '<span>Custom Web Development</span><span>HTML, CSS, JavaScript/jQuery, Responsive, Accessibility</span>',
		"caption": '<p>My portfolio site is a great sample of my work! I can\'t take credit for the <a href="http://www.templatemo.com/tm-464-ultra-profile" target="_blank">design</a> but I certainly will for all of the development. I chose not to go with a CMS because it would be overkill and is simple enough to build and update from scratch. I utilized the design elements from the original concept but redeveloped the entire site and put my own spin on a few elements. The original code heavily utilized 3rd-party resources and was not built for accessibility so I really wanted to fix that.</p><p>I rebuilt the functionality myself that was previously accomplished via 3rd-party resources. This functionality includes the responsive aspects of the site (HTML & CSS), the scroll stops (CSS & JS), and the sticky header (HTML, CSS, & JS). I rebuilt these as well as the rest of this site with accessibility in mind so I utilized rich HTML markup along with ARIA roles and attributes where necessary. This site is also fully responsive and can be viewed on any device. I built specific CSS Media Queries for 320+, 480+, 640+, and 768+.</p><p>Feel free to view the page source and browse through my code! I hope you find something that interests you! Also feel free to check out the accessibility of the site. I tested everything through keyboard interaction/navigation along with Mac VoiceOver as well as evaluating with <a href="http://wave.webaim.org/" target="_blank">WebAIM\'s WAVE tool</a> and the <a href="https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd" target="_blank">aXe Chrome Extension</a>.</p><p>Thanks for stopping by and checking out my work!</p>',
		"image": '/portfolio/images/brenton.jpg',
		"link": {
			"show": false,
			"text": '',
			"url": '',
			"target": ''
		}
	},{
		"show": true,
		"title": 'Simple Gallery',
		"subtitle": '<span>Custom JavaScript Application</span><span>HTML, CSS, JavaScript/jQuery (OOP), Responsive, Accessibility</span>',
		"caption": '<p>The Simple Gallery is the very application that you are currently viewing. I wanted to build a simple and modern image gallery to display my work samples while writing an object-oriented gallery utilizing the "prototype" property. I\'ve written countless jQuery plugins for purposes like this but I needed something a little more exciting to showcase!</p><p>The "frosted" image effect is very popular right now and developing this aspect with CSS via "filter" property was a fun challenge. I also utilized the CSS "transition" property to handle some of the transition effects along with jQuery\'s .fadeIn() and .fadeOut() methods. The CSS transitions allow me to very easily change some aspects of the animation across different viewports as well which make the gallery effectively presentable on mobile.</p><p>The gallery is fully accessible. I utilized rich HTML markup for the UI along with live ARIA attributes. I tested the gallery through keyboard interaction/navigation along with Mac VoiceOver as well as evaluating with <a href="http://wave.webaim.org/" target="_blank">WebAIM\'s WAVE tool</a> and the <a href="https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd" target="_blank">aXe Chrome Extension</a>.</p><h5>Some of the general aspects of this project:</h5><ul><li>Written entirely from scratch.</li><li>Fully responsive on any device/viewport.</li><li>Custom CSS Media queries for mobile viewports - 320+ and 640+.</li><li>Left and right swipe events for mobile devices.</li><li>Fully accessible.</li></ul>',
		"image": '/portfolio/images/simple-gallery.jpg',
		"link": {
			"show": true,
			"text": 'View<span class="accessible-hidden"> Simple Gallery Code</span> Pen »',
			"url": 'http://codepen.io/brenton-kelly/pen/RGzyyd?editors=0010#0',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Cajon Valley Union School District',
		"subtitle": '<span>Custom Template Package</span><span>HTML, CSS, JavaScript/jQuery, Responsive</span>',
		"caption": '<p>Cajon Valley Union School District\'s website is visually one of my favorites. They included a number of unique elements such as a parallax "stacking" header, a multimedia gallery that includes imagery and embeded YouTube videos, a large school list dropdown, and more. I custom coded every aspect of their template package.</p><p>Their parallax "stacking" header is illustrated when scrolling down their web pages. I coded this element with HTML, CSS, and jQuery and I engineered this solution to use a series of variables as "checks" against the window\'s scroll position to keep the number of DOM hits to a bare minimum. The use of CSS transitions througout the animations and functionality help keep the solution light while simply adding/removing classes with jQuery.</p><h5>Some of the general aspects of this project:</h5><ul><li>Designed in Adobe Illustrator.</li><li>Utilizes HTML5 elements.</li><li>Utilizes the CSS Flexbox module.</li><li>Utilizes CSS transitions for soft effects.</li><li>Custom coded the Multimedia Rotator jQuery plugin.</li><li>Generated custom icon font in IcoMoon from custom SVG\'s.</li><li>Tested and supported for all major browsers and IE9+.</li></ul>',
		"image": '/portfolio/images/cajon-valley.jpg',
		"link": {
			"show": true,
			"text": 'Visit<span class="accessible-hidden"> Cajon Valley Union School District</span> Site »',
			"url": 'http://www.cajonvalley.net/',
			"target": '_blank'
		}
	},{
		"show": false,
		"title": 'School List Management Solution',
		"subtitle": '<span>jQuery Plugin &amp; Chrome Extension</span><span>HTML, CSS, JavaScript/jQuery</span>',
		"caption": '',
		"image": '',
		"link": {
			"show": false,
			"text": '',
			"url": '',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Creative Services Meeting Request',
		"subtitle": '<span>Custom Scheduling Application &amp; Firebase Backend</span><span>HTML, CSS, JavaScript/jQuery, JSONP, ASP, Firebase, Responsive, Accessibility</span>',
		"caption": '<p>The design team that I work with frequently schedules design kick-off meetings with clients through a web-based scheduling form. The solution they were previously using was very inefficient and cumbersome in that it required updating every two days and sometimes even daily. The dates and meeting times were set manually and multiple bookings were a constant problem. Managing the solution required a lot of valuable time.</p><p>I am a huge advocate for building/improving efficiencies and creating effective workflows. When I learned of the design team\'s scheduling process, I jumped right in to help. The most important part of the project and ultimately an incredible time saver was building automation. I identified this along with many other requirements while gather the functional specifications for the project. I mapped out my tools and workflow and got started immediately.</p><p>I custom built the "Creative Services Meeting Request" application with a <a href="https://firebase.google.com/" target="_blank">Firebase</a> backend that houses all of the scheduled meetings and settings for the application. All of the data from Firebase is manipulated through their well-structured API and allows the UI to be updated live as meetings are scheduled and settings are updated. The application is highly configurable for administrators allowing them to set specific recurring and non-recurring time frames and it updates live in Eastern Time no matter what time zone the user is in. I used <a href="http://momentjs.com/" target="_blank">Moment.js</a> to handle time in the application given the importance of keeping everything in the Eastern Time zone. When a meeting request is submitted, an email is sent to the configured email address(es) in the application settings notifying them of the submission. The information is sent to a custom ASP email script via JSONP as the ASP server is cross-domain.</p><p>The application is responsive and can be viewed on any device. I used custom CSS Media Queries to make minor adjustments for small viewports. The application is also fully accessible. I utilized well-structured HTML markup for the UI and I tested the application through keyboard interaction/navigation along with Mac VoiceOver as well as evaluating with <a href="http://wave.webaim.org/" target="_blank">WebAIM\'s WAVE tool</a> and the <a href="https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd" target="_blank">aXe Chrome Extension</a>.</p><p>The time savings in managing the new solution as been huge for the designers. They have reported updating the previous solution a total of five to eight hours a week. With the new solution, they have reported a total of zero to 15 minutes a week. That\'s between a 95% to 100% time savings.</p>',
		"image": '/portfolio/images/meeting-request.jpg',
		"link": {
			"show": true,
			"text": 'View<span class="accessible-hidden"> Creative Services Meeting Request</span> Application »',
			"url": 'http://cerc.blackboard.com/CreativeServicesMeeting',
			"target": '_blank'
		}
	},{
		"show": false,
		"title": 'CodeMirror Extension',
		"subtitle": '<span>Chrome Extension</span><span>HTML, CSS, JavaScript/jQuery</span>',
		"caption": '',
		"image": '',
		"link": {
			"show": false,
			"text": '',
			"url": '',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Auburn City Schools',
		"subtitle": '<span>Custom Vimeo Asset</span><span>HTML (Canvas), CSS, JavaScript/jQuery, Vimeo API</span>',
		"caption": '<p>I had a unique role in Auburn City Schools\'s custom template package project. My task was to develop a dynamic "play/pause progress loader" for their embedded Vimeo video on the homepage of their website.</p><p>At the time, I chose to use the <a href="https://github.com/piscis/vimeo-froogaloop2" target="_blank">Froogaloop</a> library to interact with the Vimeo player because it makes it very easy and a lot like working with the YouTube API, which I\'m already comfortable with. I first attempted to work with a CSS3 animation for the progress indicator but found it quite cumbersome when the play/pause button was invoked. It was easy for the progress timing to get out of sync with the video. I needed something even more dynamic and something that would preferably follow the lead of the video whether it was playing or paused.</p><p>I read more into Vimeo\'s documentation and found that like YouTube, Vimeo offers a "progress" event that constantly feeds the player information. In my case, I grabbed the "percent loaded" information and ultimately used it to keep my progress animation up-to-date. This is perfect because whether or not the video is paused, the information coming from the API is always correct.</p><p>For the animated progress element, I used HTML5 canvas. I\'ve used canvas many times before and knew that it would ultimately be a better result than trying to build something with CSS given how the actual element behaves. I was able to write a very lightweight solution to get the video\'s "percent loaded" information, make some calculations, and draw the canvas element. The canvas is continuously redrawn throughout the duration of the video and provides the animated effect.</p><p>You can view this solution on <a href="http://auburn.schoolwires.net/" target="_blank">Auburn City Schools\'s</a> site or take a look at the example I recreated on CodePen via the link below.</p>',
		"image": '/portfolio/images/auburn.jpg',
		"link": {
			"show": true,
			"text": 'View<span class="accessible-hidden"> Custom Vimeo Progress Loader</span> Pen »',
			"url": 'http://codepen.io/brenton-kelly/pen/jVNLRP',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'VAMP WMS',
		"subtitle": '<span>Custom Website Management System</span><span>HTML, CSS, JavaScript/jQuery, AJAX, PHP, MySQL, Responsive</span>',
		"caption": '<p>VAMP is a website management system which allows users to fully control every single aspect of their website. I built the system to be very configurable and able to create custom applications on the fly from within the system. The driving idea was that I wanted a system that was very easy to use and one that I could personally support for my freelance clients. With VAMP WMS, users can create and edit pages and navigation, sort pages and navigation, manage website content, upload and manage images and files, manage back-end users, develop website templates, develop custom page layouts, and develop custom content regions.</p><p>I built VAMP WMS entirely from scratch and I published the first version in January 2012. It was a huge undertaking but a great learning experience. It allowed me to grow both my front-end and server-side development skills exponentially and since the first version, I\'ve been working through updates to improve the experience and functionality. Over the last few years, I\'ve learned a lot about object-oriented programming in both JavaScript and PHP. I\'ve been planning a rebuild of the system using OOP to make it more robust, more scalable, and easier to maintain. Time permitting, I\'m hoping to have the opportunity to complete the task over the next few years. I\'d be happy to talk specifics about my system at any time as there\'s just far too much information to provide in this limited space!</p><p>So far, my clients have reported being very happy with the system and have reported very few issues with it. I\'ve been very responsive in applying updates as necessary. I built VAMP to be a single, centralized system so that I only have to apply updates in one place. One of my favorite websites powered by VAMP WMS is <a href="http://jilco-inc.com/" target="_blank">Jilco, Inc</a>. Check them out!</p>',
		"image": '/portfolio/images/vwms.jpg',
		"link": {
			"show": true,
			"text": 'Visit<span class="accessible-hidden"> the VAMP WMS</span> Site »',
			"url": 'http://www.vampwms.com',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Multimedia Rotator',
		"subtitle": '<span>Custom jQuery Plugin</span><span>HTML, CSS, JavaScript/jQuery, Responsive, Accessibility</span>',
		"caption": '<p>I wrote the custom "Multimedia Rotator" plugin completely from scratch. It pulls its data from a multidimensional array which is generated from a custom application that I built within the CMS that I develop for. It allows a user to display images with title and caption text along with custom links if they chose. The rotator also allows a user to embed videos from YouTube, Vimeo, TeacherTube, SchoolTube, and Kaltura that display as a pop-up modal window. Embedded YouTube and Vimeo videos offer auto-play and auto-pause functionality through their API\'s.</p><p>The rotator is fully responsive and customizable across viewports. It accepts width and height properties that the plugin uses to continuously calculate the proper dimensions based on the given ratio from these properties. There are also properties that set the rotator to match the users full viewport for a full-screen gallery as well as run custom functions upon different set events from with the plugin such as when a single image is loaded, when a transition starts and/or ends, when all rotator content is loaded, etc. The rotator provides "fade" and "slide" transitions that, along with CSS, can be further customized to match a user\'s preferences.</p><p>The Multimedia Rotator plugin is fully accessible. It utilizes various ARIA attributes and tabindexes to provide proper navigation and functionality for keyboard users and those using screen readers and other assistive technologies. I tested the gallery through keyboard interaction/navigation along with Mac VoiceOver as well as evaluating with <a href="http://wave.webaim.org/" target="_blank">WebAIM\'s WAVE tool</a> and the <a href="https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd" target="_blank">aXe Chrome Extension</a>.</p><p><a href="http://codepen.io/brenton-kelly/pen/VmwZrq/?editors=0010" target="_blank">Here is a CodePen</a> with the production code where you can see all of the available options and functionality.</p>',
		"image": '/portfolio/images/mmr.jpg',
		"link": {
			"show": true,
			"text": 'View <span class="accessible-hidden"> Multimedia Rotator</span> Example »',
			"url": 'http://rps1.schoolwires.net/',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Alief Independent School District',
		"subtitle": '<span>Custom Template Package</span><span>HTML, CSS, JavaScript/jQuery, Responsive, Accessibility</span>',
		"caption": '<p>Alief Independent School District is another one of my favorites. Their custom template houses a lot of complex and interactive functionality. Like Cajon Valley USD, Alief utilizes a stacking sticky header during scroll progression that keeps navigation with the user. A very simple but elegant effect.</p><p>One of Alief\'s complex elements is their mega school menu. I wrote the custom jQuery plugin "schoolListMinibase()" that retrieves the school data and packages it as an array of objects for use in the "schoolListReady" callback method. The user inputs the school information into the CMS and I generate the dynamic menu based on those values. The tabs are fully configurable as well as each site in the lists and the entire application is tucked into the responsive main menu on mobile viewports.</p><p>Another one of Alief\'s complex elements is the large tabbed area under the homepage slideshow region. These four tabs are identified in the navigation structure via "resources" keyword and then dynamically relocated to this region and reconfigured as an accessible tabbed application. The user has the ability to input an accent image in each panel along with some additional information along side the navigation links. </p><p>I custom coded the entire template package. The template looks and functions great in any viewport. The template is also fully accessible. I utilized rich HTML markup for the UI along with ARIA roles and attributes as necessary. I tested the site through keyboard interaction/navigation along with Mac VoiceOver as well as evaluating with <a href="http://wave.webaim.org/" target="_blank">WebAIM\'s WAVE tool</a> and the <a href="https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd" target="_blank">aXe Chrome Extension</a>. (CMS and client based issues did not apply)</p><h5>Some of the general aspects of this project:</h5><ul><li>Built for Blackboard\'s K12 CMS: Web Community Manager.</li><li>Designed in Adobe Illustrator.</li><li>Utilizes HTML5 elements.</li><li>Utilizes the CSS Flexbox module.</li><li>Utilizes CSS transitions for soft effects.</li><li>Fully accessible.</li><li>Generated custom icon font in IcoMoon from custom SVG\'s.</li><li>Minified JavaScript assets and compressed template assets with Kraken.io.</li><li>Tested and supported for all major browsers and IE9+.</li></ul>',
		"image": '/portfolio/images/alief.jpg',
		"link": {
			"show": true,
			"text": 'Visit<span class="accessible-hidden"> Alief Independent School District</span> Site »',
			"url": 'http://www.aliefisd.net/',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Creative Services Image Editor',
		"subtitle": '<span>Custom Image Editing Application</span><span>HTML, CSS, JavaScript/jQuery</span>',
		"caption": '<p>I custom built the "Creative Services Image Editor" so that the Creative Services team can provide clients a simple solution to a recurring problem. Clients often need to crop and resize imagery for their websites and many struggle with this task given that there aren\'t any simple editors available with only a small set of options that they need. Ease-of-use is a huge part of this struggle.</p><p>I custom built the image editor using <a href="http://scottcheng.github.io/cropit/" target="_blank">cropit.js</a> as a foundation. The application resizes, crops, rotates, and mirrors. Cropit doesn\'t include a "mirror" option so I built it into the existing plugin. A client can set the dimensions they want to size their image(s) to and when uploaded, the user can drag the image around in the viewer to set the cropped area. The application will remember the dimensions that the user sets via a cookie. Also, when sharing the link to the application, you can preset the dimensions in the query string so the user is ready to go upon page load.</p><p>When an image is prepared for download, the image information is retrieved through the cropit plugin as a base64 data URI and converted to a Blob object which is then applied to the "Download Cropped Image" button as an "href" attribute. The download is ultimately triggered upon click via the "download" attribute on the button. Please feel free to visit the application and view the page source to see how I wrote it! Aside from the cropit.js plugin, it was all from scratch.</p><p>The Creative team has received a lot of great feedback from clients. They love how easy the image editor is to use!</p>',
		"image": '/portfolio/images/image-editor.jpg',
		"link": {
			"show": true,
			"text": 'View<span class="accessible-hidden"> Creative Services Image Editor</span> Application »',
			"url": 'http://templatelibrary.schoolwires.net/imageEditor',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Events Calendar',
		"subtitle": '<span>jQuery Plugin</span><span>HTML, CSS, JavaScript/jQuery, AJAX, Responsive</span>',
		"caption": '<p>Sumner School District included a very unique element in their design. They wanted to take an application that displays an ordinary list of events and reconfigure it into an interactive calendar. No one had done this before so it was a fun challenge to take on. Given the unique functionality and UI of this application, I decided to build it into a jQuery plugin so that it could also be used by other clients.</p><p>The calendar gets it\'s initial list of events right from the CMS for the current month. The "eventsCalendar()" plugin then builds the responsive 50/50 view with the calendar on the left and event viewer on the right. A user can navigate through past months or future months to view more events. When this navigation happens, an AJAX request queries the set of events for the given month through a "GetEvents" service within the CMS. The returned data is an object that is used to rebuilt the view and the data is stored in a cookie so that the AJAX request doesn\'t have to take place again. When a particular day is clicked in the calendar, all events for the day are displayed in the event viewer. The user can click on an event to read more and can also share the event via Facebook and Twitter.</p><p>Sumner School District is using this application on the home page of their website under "Events". Check it out! Also feel free to <a href="http://codepen.io/brenton-kelly/pen/oYgMwV?editors=0010#0" target="_blank">take a look at the CodePen</a> with the production code.</p>',
		"image": '/portfolio/images/events-calendar.jpg',
		"link": {
			"show": true,
			"text": 'View<span class="accessible-hidden"> Events Calendar</span> Application »',
			"url": 'http://www.sumnersd.org/',
			"target": '_blank'
		}
	},{
		"show": true,
		"title": 'Richmond Public Schools',
		"subtitle": '<span>Custom Template Package</span><span>HTML, CSS, JavaScript/jQuery, YouTube API, Accessibility</span>',
		"caption": '<p>Richmond Public Schools chose a design with many interactive elements that include a toggle between a YouTube background video using the YouTube API and a jQuery plugin that I wrote called the Multimedia Rotator, a select-a-school tab panel menu, a mega search modal, and a responsive home page image collage that I developed with 100% CSS. I developed some of these areas and many others using the CSS Flexbox module which gave me more control over the extremely dynamic aspects of the elements. I custom coded the entire template package and met their extremely tight deadline for developing and launching the solution.</p><p>My favorite part of this project was the CSS image collage. I had never developed an element like it before and I loved the challenge of making it fully responsive while maintaining image proportions without a single line of JavaScript. Utilizing a before pseudo-element on the main container with a calculated top padding was the key. Percentage based padding is relative to the width of the element it\'s applied to so all I had to do was calculate the overall ratio and apply it as a percentage for "padding-top". The scaling and re-ordering of the element across all viewports is smooth and completely seamless. I was sure to make the element accessible for assistive technology users as well.</p><p>Richmond Public Schools\' template is fully accessible. I utilized rich HTML markup for the UI along with ARIA roles and attributes as necessary. I tested the site through keyboard interaction/navigation along with Mac VoiceOver as well as evaluating with <a href="http://wave.webaim.org/" target="_blank">WebAIM\'s WAVE tool</a> and the <a href="https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd" target="_blank">aXe Chrome Extension</a>. (CMS and client based issues did not apply)</p><h5>Some of the general aspects of this project:</h5><ul><li>Built for Blackboard\'s K12 CMS: Web Community Manager.</li><li>Utilizes HTML5 elements.</li><li>Utilizes the CSS Flexbox module.</li><li>Utilizes CSS transitions for effects.</li><li>Responsive: Media queries set for 320+, 480+, 640+, and 768+.</li><li>Generated custom icon font in IcoMoon from custom SVG\'s.</li><li>Minified JavaScript assets and compressed template assets with Kraken.io.</li><li>Tested and supported for all major browsers and IE11+.</li></ul>',
		"image": '/portfolio/images/richmond.jpg',
		"link": {
			"show": true,
			"text": 'Visit<span class="accessible-hidden"> Richmond Public Schools</span> Site »',
			"url": 'http://www.rvaschools.net/',
			"target": '_blank'
		}
	}
];