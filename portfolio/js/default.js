var navigationIsSticky = 0;
var bkKeyCodes = { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 };

$(window).resize(function() { stickyNav(); navigationScroll(); });
$(window).scroll(function() { stickyNav(); navigationScroll(); });
$(window).load(function() { navigationScroll(); });
	
$(function() {
	stickyNav();
	navigationScroll();
	defaultJS();
	
	var simpleGallery = new SimpleGallery({
		"config": {
			"container": "#bk-portfolio-samples",
			"titleWrapElement": "h4",
			"captionWrapElement": "div",
			"width": 1500,
			"height": 675,
			"animationSpeed": 750,
        	"animationDelay": 8000
		},
		"records": galleryData
	});
});

function defaultJS() {
	// NAVIGATION CLICK
	$(document).on("click keydown", "#bk-welcome-start, .bk-nav-link", function(e) {
		if(e.type == "click" || e.type == "keydown" && e.keyCode == bkKeyCodes.space || e.type == "keydown" && e.keyCode == bkKeyCodes.enter) {
        	e.preventDefault();
			if($(this).hasClass("bk-nav-link")) {
				$("#bk-nav ul").toggleClass("open");
			}
			
			var scrollContainer = $(this).attr("data-container");
			$("html, body").animate({
				scrollTop: $("#" + scrollContainer).offset().top + 5
			}, 500, function(){
				$("html, body").stop(true, false);
			});
		}
	});
	
	// NAVIGATION FOCUS
	$("#bk-nav ul li a").focus(function() {
		if(!$("#bk-nav-outer").hasClass("visible")) {
			$("html, body").animate({
				scrollTop: 5
			}, 500, function(){
				$("html, body").stop(true, false);
			});
		}
	});
	
	// MOBILE NAVIGATION CLICK
	$(document).on("click keydown", "#bk-mobile-nav", function(e) {
		if(e.type == "click" || e.type == "keydown" && e.keyCode == bkKeyCodes.space || e.type == "keydown" && e.keyCode == bkKeyCodes.enter) {
        	e.preventDefault();
			$("#bk-nav ul").toggleClass("open");
		}
	});
	
	// SAMPLE HOVER
	$(".bk-portfolio-sample a").hover(function() {
		$(".bk-portfolio-sample-preview", this).attr("aria-hidden", (($(".bk-portfolio-sample-preview", this).attr("aria-hidden") == "true") ? "false" : "true"));
	}, function() {
		$(".bk-portfolio-sample-preview", this).attr("aria-hidden", (($(".bk-portfolio-sample-preview", this).attr("aria-hidden") == "true") ? "false" : "true"));
	});
	
	// SAMPLE FOCUS
	$(".bk-portfolio-sample a").focus(function() {
		$(".bk-portfolio-sample-preview", this).attr("aria-hidden", (($(".bk-portfolio-sample-preview", this).attr("aria-hidden") == "true") ? "false" : "true"));
	}).blur(function() {
		$(".bk-portfolio-sample-preview", this).attr("aria-hidden", (($(".bk-portfolio-sample-preview", this).attr("aria-hidden") == "true") ? "false" : "true"));
	});
	
	// CONTACT FORM
	$(".bk-form-input").focus(function() {
		if($(this).val() == "Please fill out this field") {
			$(this).val("").css("border", "1px solid #666");	
		}
	});
	
	$("#bk-contact-form").submit(function(event){
		event.preventDefault();
		
		// SET UP VARIABLES
		var contactName = $(".bk-form-input.name").val();
		var contactEmail = $(".bk-form-input.email").val();
		var contactSubject = $(".bk-form-input.subject").val();
		var contactMsg = $(".bk-form-input.message").val();
		var hasError = 0;
		
		$(".bk-form-input").each(function() {
			if($(this).val() == "") {
				$(this).val("Please fill out this field").css("border", "1px solid #990000");
				hasError = 1;
			}
		});
		
		if(!hasError) {
			$(".bk-form-input.send").addClass("sending-mail").val("SHOOTING...");
			
			var postData = { 
				name: contactName,
				email: contactEmail,
				subject: contactSubject,
				message: contactMsg
			}
			
			// AJAX THE INFO TO THE PHP SCRIPT
			$.post("/portfolio/php/sendmail.php", postData, function(data) {
				if(data == "SUCCESS"){
					$("#bk-contact-form").remove();
					$("#bk-contact").append("<p id='success-mail-msg'>Thank you! Your message has been sent successfully!</p>");
				} else if(data == "FAIL"){
					$(".bk-form-input.send").removeClass("sending-mail").val("SHOOT MESSAGE");
					// DISPLAY ERROR ALERT
					alert("An unknown error has occured. Please try to send your message again. If this error continues, please contact Brenton Kelly directly at b.kelly@brentonskelly.com");
				} else {
					$(".bk-form-input.send").removeClass("sending-mail").val("SHOOT MESSAGE");
					// DISPLAY ERROR ALERT
					var formattedData = data.replace(/(<([^>]+)>)/ig,"");
					alert("There was an unexpected error! If this problem continues, please contact Brenton Kelly directly at b.kelly@brentonskelly.com");
				}
			});
		}
	});
}

function stickyNav() {
	var navOffset = $("#bk-page").offset().top + 1;
	if ($(window).scrollTop() <= navOffset) {
		if(navigationIsSticky) {
			$("#bk-nav-outer").removeClass("visible");
			navigationIsSticky = 0;
		}
	} else {
		if(!navigationIsSticky) {
			$("#bk-nav-outer").addClass("visible");
			navigationIsSticky = 1;
		}
	}
}

function navigationScroll() {
	// HOME ACTIVE
	if($(window).scrollTop() < $("#bk-work-outer").offset().top) {
		setLinkState("home");
	}
	
	// MY WORK ACTIVE
	if($(window).scrollTop() >= $("#bk-work-outer").offset().top && $(window).scrollTop() < $("#bk-portfolio-outer").offset().top) {
		setLinkState("my-work");
	}
	
	// PORTFOLIO ACTIVE
	if($(window).scrollTop() >= $("#bk-portfolio-outer").offset().top && $(window).scrollTop() < $("#bk-profile-outer").offset().top) {
		setLinkState("portfolio");
	}
	
	// PROFILE ACTIVE
	if($(window).scrollTop() >= $("#bk-profile-outer").offset().top && $(window).scrollTop() < $("#bk-bio-outer").offset().top) {
		setLinkState("profile");
		$(".bk-skill-bar").removeClass("load");
	}
	
	// ABOUT ACTIVE
	if($(window).scrollTop() >= $("#bk-bio-outer").offset().top && $(window).scrollTop() < $("#bk-contact-outer").offset().top) {
		setLinkState("about");
	}
	
	// CONTACT ACTIVE
	if($(window).scrollTop() >= $("#bk-contact-outer").offset().top && $(window).scrollTop() < $("footer").offset().top) {
		setLinkState("contact");
	}
}

function setLinkState(linkClass) {
	if(!$(".bk-nav-link." + linkClass).hasClass("active")) {
		$(".bk-nav-link.active").removeClass("active");
		$(".bk-nav-link." + linkClass).addClass("active");
	}
}

function SimpleGallery(gallery) {
	// SET THIS INSTANCE VARIABLES
	this.galleryContainer = gallery.config.container;
	this.galleryTitleWrapElement = gallery.config.titleWrapElement;
	this.galleryCaptionWrapElement = gallery.config.captionWrapElement;
	this.galleryWidth = gallery.config.width;
	this.galleryHeight = gallery.config.height;
	this.galleryAnimationSpeed = gallery.config.animationSpeed;
	this.galleryAnimationDelay = gallery.config.animationDelay;
	this.galleryRecords = gallery.records;
	this.galleryHover = false;
	
	var d = new Date();
	this.galleryTimerID = d.getTime();
	this.galleryTimer = [];
	
	this.galleryKeyCodes = {
		"tab"	: 9,
		"enter"	: 13,
		"esc"	: 27,
		"space"	: 32,
		"end"	: 35,
		"home"	: 36,
		"left"	: 37,
		"up"	: 38,
		"right"	: 39,
		"down"	: 40
	};
	
	// GET THINGS STARTED
	this.buildStyles();
	this.init();
}

SimpleGallery.prototype.init = function() {
	// START BUILDING THE GALLERY STRUCTURE AND BULLETS
	var galleryStructure = '<div class="simple-gallery-viewer">';
	var galleryBullets = '<ul class="simple-gallery-bullets clear">';
	
	// LOOP THROUGH RECORDS AND BUILD THE PANELS AND BULLETS
	var iterator = 0;
	for(var record in this.galleryRecords) {
		// IF THIS RECORD IS SET TO SHOW
		if(this.galleryRecords[record].show) {
			// PANEL
			// MARK ACTIVE AND VISIBLE IF THIS IS THE FIRST RECORD
			galleryStructure +=	'<article class="simple-gallery-record' + ((iterator == 0) ? " active reveal" : "") + '" data-index="' + iterator + '" aria-hidden="' + ((iterator == 0) ? "false" : "true") + '" style="background: url(' + this.galleryRecords[record].image + ') center center / cover no-repeat;">';
			galleryStructure +=		'<div class="simple-gallery-record-inner">';
			galleryStructure +=			'<div class="simple-gallery-record-frost-image" role="img" aria-label="Gallery image for ' + this.galleryRecords[record].title + '" style="background-image: url(' + this.galleryRecords[record].image + ');"></div>';
			galleryStructure +=			'<div class="simple-gallery-record-info">';
			galleryStructure +=				'<div class="simple-gallery-record-info-inner">';
												// GIVE ABILITY TO SET ELEMENT TYPE FOR MORE CONTROL, PROPER HIERARCHY, AND ACCESSIBILITY REASONS
			galleryStructure +=					'<' + this.galleryTitleWrapElement + ' class="simple-gallery-record-header">' + this.galleryRecords[record].title + this.galleryRecords[record].subtitle + '</' + this.galleryTitleWrapElement + '>';
												// GIVE ABILITY TO SET ELEMENT TYPE FOR MORE CONTROL AND ACCESSIBILITY REASONS
												// MAY WANT TO PUT BLOCK LEVEL ELEMENTS IN HERE SO FORCING <p> WOULDN'T BE COOL
			galleryStructure +=					'<' + this.galleryCaptionWrapElement + ' class="simple-gallery-record-caption">' + this.galleryRecords[record].caption + '</' + this.galleryCaptionWrapElement + '>';
			if(this.galleryRecords[record].link.show) {
			galleryStructure +=					'<p class="simple-gallery-record-link"><a href="' + this.galleryRecords[record].link.url + '" target="' + this.galleryRecords[record].link.target + '">' + this.galleryRecords[record].link.text + '</a></p>';
			}
			galleryStructure +=				'</div>';
			galleryStructure +=			'</div>';
			galleryStructure +=		'</div>';
			galleryStructure += '</article>';

			// BULLET
			// MARK ACTIVE IF THIS IS THE FIRST RECORD
			galleryBullets += '<li class="simple-gallery-bullet"><button class="simple-gallery-bullet-btn' + ((iterator == 0) ? ' active' : '') + '" data-index="' + iterator + '" aria-label="Navigate to ' + this.galleryRecords[record].title + ' slide"></button></li>';
		
			iterator++;
		}
		
		// SKIP THIS ITERATION
		else {
			continue;
		}
	}
	
	// FINISH THE GALLERY STRUCTURE AND BULLETS
	galleryStructure += '</div>';
	galleryBullets += '</ul>';

	// ADD THE STRUCTURE TO THE DOM
	$(this.galleryContainer).html(galleryBullets + galleryStructure);
	
	// START ROTATING THE SLIDES
	this.setTimer();
	
	// SET EVENTS
	this.setEvents();
}

SimpleGallery.prototype.setEvents = function() {
	// FOR SCOPE
	var _this = this;
	
	// BULLET ACTION
	$(this.galleryContainer).on("click keydown", ".simple-gallery-bullet-btn", function(e) {
		if(e.type == "click" || e.type == "keydown" && e.keyCode == _this.galleryKeyCodes.space || e.type == "keydown" && e.keyCode == _this.galleryKeyCodes.enter) {
			e.preventDefault();

			if(!$(this).hasClass("active")) {
				_this.clearTimer();
				_this.triggerAction($(this).attr("data-index"));
			}
		}
	});
	
	// GALLERY SWIPE LEFT
	$(this.galleryContainer).on("swipeleft",function(){
		var nextIndex;

		// IF LAST CHILD IS CURRENTLY ACTIVE
		if($(".simple-gallery-record.active", this.galleryContainer).is(":last-child")) {
			nextIndex = 0;
		}

		// LAST CHILD IS NOT CURRENTLY ACTIVE
		else {
			nextIndex = $(".simple-gallery-record.active", this.galleryContainer).next(".simple-gallery-record").attr("data-index");
		}
		
		_this.clearTimer();
		_this.triggerAction(nextIndex);
	});
	
	// GALLERY SWIPE RIGHT
	$(this.galleryContainer).on("swiperight",function(){
		var nextIndex;

		// IF FIRST CHILD IS CURRENTLY ACTIVE
		if($(".simple-gallery-record.active", this.galleryContainer).is(":first-child")) {
			nextIndex = $(".simple-gallery-record:last-child", this.galleryContainer).attr("data-index");
		}

		// FIRST CHILD IS NOT CURRENTLY ACTIVE
		else {
			nextIndex = $(".simple-gallery-record.active", this.galleryContainer).prev(".simple-gallery-record").attr("data-index");
		}
		
		_this.clearTimer();
		_this.triggerAction(nextIndex);
	});
	
	// GALLERY HOVER PAUSE
	$(this.galleryContainer).hover(function() {
		_this.clearTimer();
		
		$(this).addClass("hover");
		_this.galleryHover = true;
	}, function() {
		_this.clearTimer();
		_this.setTimer();
		
		$(this).removeClass("hover");
		_this.galleryHover = false;
	});
	
	// GALLERY FOCUS PAUSE
	$(document).keyup(function() {
		// FOCUS IS INSIDE THE GALLERY
		if($(_this.galleryContainer).find(":focus").length) {
			_this.clearTimer();
			
			$(_this.galleryContainer).addClass("hover");
			_this.galleryHover = true;
		}
		
		// FOCUS IS SOMEWHERE OUTSIDE THE GALLERY
		else {
			if(_this.galleryHover) {
				_this.clearTimer();
				_this.setTimer();

				$(_this.galleryContainer).removeClass("hover");
				_this.galleryHover = false;
			}
		}
	});
	
	// MAKE SURE GALLERY RESUMES IF FOCUS IS REMOVED VIA CLICK
	$(document).click(function() {
		// FOCUS IS SOMEWHERE OUTSIDE THE GALLERY
		if(!$(_this.galleryContainer).find(":focus").length) {
			if(_this.galleryHover) {
				_this.clearTimer();
				_this.setTimer();

				$(_this.galleryContainer).removeClass("hover");
				_this.galleryHover = false;
			}
		}
	});
	$(this.galleryContainer).click(function(e) {
		e.stopPropagation();
	});
}

SimpleGallery.prototype.setAttributes = function() {
	$(".simple-gallery-record", this.galleryContainer).each(function() {
		// ACTIVE PANEL
		if($(this).hasClass("active")) {
			$(this).attr("aria-hidden", "false");
			$("a", this).attr("tabindex", "0");
		}

		// HIDDEN PANEL
		else {
			$(this).attr("aria-hidden", "true");
			$("a", this).attr("tabindex", "-1");
		}
	});
}

SimpleGallery.prototype.setTimer = function() {
	// FOR SCOPE
	var _this = this;

	this.galleryTimer[this.galleryTimerID] = setInterval(function() {
		var nextIndex;

		// IF LAST CHILD IS CURRENTLY ACTIVE
		if($(".simple-gallery-record.active", this.galleryContainer).is(":last-child")) {
			nextIndex = 0;
		}

		// LAST CHILD IS NOT CURRENTLY ACTIVE
		else {
			nextIndex = $(".simple-gallery-record.active", this.galleryContainer).next(".simple-gallery-record").attr("data-index");
		}

		_this.triggerAction(nextIndex);
	}, this.galleryAnimationDelay);
}

SimpleGallery.prototype.clearTimer = function() {
	clearInterval(this.galleryTimer[this.galleryTimerID]);
}

SimpleGallery.prototype.triggerAction = function(nextIndex) {
	// FOR SCOPE
	var _this = this;

	// STOP CURRENT ANIMATION
	$(".simple-gallery-record", this.galleryContainer).stop(true, true);

	// ANIMATE THE PANELS
	// CREATE A PROMISE SO WE CAN RESET ATTRIBUTES ONLY AFTER EVERYTHING IS DONE ANIMATING
	$.when(
		// FADE OUT CURRENT ACTIVE
		$(".simple-gallery-record.active", this.galleryContainer).removeClass("reveal").delay(this.galleryAnimationSpeed).fadeOut(this.galleryAnimationSpeed, function() {
			$(this).removeClass("active");
			$(".simple-gallery-bullet-btn.active", this.galleryContainer).removeClass("active");
		}),
		
		// FADE IN NEW ACTIVE
		$(".simple-gallery-record[data-index='" + nextIndex + "']", this.galleryContainer).delay(this.galleryAnimationSpeed).fadeIn(this.galleryAnimationSpeed, function() {
			$(this).addClass("active reveal");
			$(".simple-gallery-bullet-btn[data-index='" + nextIndex + "']", this.galleryContainer).addClass("active");
		})
	).done(function() {
		// UPDATE ATTRIBUTES
		_this.setAttributes();
	});
}

SimpleGallery.prototype.buildStyles = function() {
	var dynStyleSheet = document.createElement('style');

	if(dynStyleSheet) {
		dynStyleSheet.setAttribute('type', 'text/css');
		var head = document.getElementsByTagName('head')[0];

		if(head) {
			head.insertBefore(dynStyleSheet, head.childNodes[0]);
		}
			
		var dynStyles =	this.galleryContainer + ' {\
							position: relative;\
						}\
						' + this.galleryContainer + ':before {\
							content: "";\
							display: block;\
							padding-top: ' + ((this.galleryHeight / this.galleryWidth) * 100) + '%; /* Ratio for ' + this.galleryWidth + ' x ' + this.galleryHeight + ' */\
						}';

		var rules = document.createTextNode(dynStyles);

		if(dynStyleSheet.styleSheet){ // IE
			dynStyleSheet.styleSheet.cssText = dynStyles;
		} else {
			dynStyleSheet.appendChild(rules);
		}
	}
}

function csGetBreakPoint() {
	return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");
}