<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Brenton S. Kelly / Web Developer</title>
    <link href="/portfolio/css/default.css" rel="stylesheet" type="text/css" />
    <link href="/portfolio/css/mobile.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Raleway:400,600,700,300" rel="stylesheet" type="text/css" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/portfolio/js/jquery.mobile.custom.min.js"></script>
    <script type="text/javascript" src="/portfolio/js/head.min.js"></script>
	<script type="text/javascript" src="/portfolio/js/gallery-data.js"></script>
    <script type="text/javascript" src="/portfolio/js/default.js"></script>
</head>

<body>
	<div id="bk-page">
    	<nav>
        	<section id="bk-nav-outer" class="bk-section-parent border-box">
            	<div id="bk-nav" class="bk-section clear">
                    <h2>Brenton Kelly</h2>
                    <i id="bk-mobile-nav" class="fa fa-bars border-box" role="button" tabindex="0"></i>
                    <ul role="menubar">
                    	<li><a role="menuitem" href="#" class="bk-nav-link home" data-container="bk-page">Home</a></li>
                        <li><a role="menuitem" href="#" class="bk-nav-link my-work" data-container="bk-work-outer">Work</a></li>
                        <li><a role="menuitem" href="#" class="bk-nav-link portfolio" data-container="bk-portfolio-outer">Portfolio</a></li>
                        <li><a role="menuitem" href="#" class="bk-nav-link profile" data-container="bk-profile-outer">Profile</a></li>
                        <li><a role="menuitem" href="#" class="bk-nav-link about" data-container="bk-bio-outer">About</a></li>
                        <li><a role="menuitem" href="#" class="bk-nav-link contact" data-container="bk-contact-outer">Contact</a></li>
                    </ul>
                </div>
            </section>
        </nav>
        <header>
        	<section id="bk-welcome-outer" class="bk-section-parent border-box">
            	<div id="bk-welcome" class="bk-section">
                	<div id="bk-welcome-inner">
                        <h1>Brenton Kelly</h1>
                        <h2>Web Developer</h2>
                        <p>I am a professional <strong>Front-End Developer</strong> creating effective, modern, responsive, and accessible web presences.</p>
                        <a href="#" id="bk-welcome-start" data-container="bk-work-outer" role="button">Let's Go!</a>
                    </div>
                </div>
            </section>
        </header>
        <main>
        	<section id="bk-main-overlay">
                <section id="bk-work-outer" class="bk-section-parent border-box">
                    <div id="bk-work" class="bk-section">
                        <h2>My <span>Work</span></h2>
                        <div class="bk-articles clear">
                            <article>
                            	<i class="bk-work-icon html fa fa-html5"></i>
                                <h3>HTML, CSS, JavaScript</h3>
                                <p>Develop advanced Front-End solutions utilizing the most modern techniques and standards in HTML, CSS, JavaScript/jQuery, AJAX, etc. Design and develop custom jQuery plugins for faster and more organized functionality.</p>
                            </article>
                            <article>
                            	<i class="bk-work-icon mobile fa fa-mobile"></i>
                                <h3>Responsive</h3>
                                <p>Develop fully responsive solutions viewable on any desktop or mobile device. Custom develop all aspects with an emphasis on usability and utilization of 3rd-party tools like head.js for feature detection and resource loading.</p>
                            </article>
                            <article>
                            	<i class="bk-work-icon mysql fa fa-database"></i>
                                <h3>PHP, MySQL</h3>
                                <p>Develop custom Back-End solutions with PHP and MySQL to maximize product outcomes. Closely tie Back-End to Front-End through the use of AJAX requests which makes the end-user experience more interactive and streamlined.</p>
                            </article>
                        </div>
                    </div>
                </section>
                <section id="bk-portfolio-outer" class="border-box">
                    <div id="bk-portfolio">
                    	<h3>My <span>Portfolio</span></h3>
                      	<div id="bk-portfolio-samples"></div>
                    </div>
                </section>
                <section id="bk-profile-outer" class="bk-section-parent border-box">
                    <div id="bk-profile" class="bk-section clear">
                    	<div class="bk-profile left">
                        	<h2>My <span>Profile</span></h2>
                            <article>
                            	<p><strong>Creative Development Manager</strong> / Blackboard</p>
                                <p><span>April 2014 - Present</span></p>
                            	<p>50% management / 50% individual contributor <span class="orange-txt">&bull;</span> Manage a team of front-end web developers as well as the execution of development projects and scheduling of service delivery <span class="orange-txt">&bull;</span> Develop and improve creative delivery with a view to scalability <span class="orange-txt">&bull;</span> Develop and improve development and workflow efficiencies <span class="orange-txt">&bull;</span> Develop custom responsive web templates and applications using the most modern development techniques involving HTML, CSS, and JavaScript/jQuery</p>
							</article>
                            <article>
                            	<p><strong>Web Developer</strong> / Schoolwires</p>
                                <p><span>May 2011 - April 2014</span></p>
                                <p>Develop custom responsive web templates and applications using the most modern development techniques involving HTML, CSS, and JavaScript/jQuery <span class="orange-txt">&bull;</span> Recognized with Amber Wolfe by the Client Services Vice President and the Client Services Senior Manager as the pioneers of Schoolwires Creative responsive product <span class="orange-txt">&bull;</span> Developed Schoolwires corporate responsive website with 12 custom applications and four custom jQuery plugins <span class="orange-txt">&bull;</span> Go-to developer for advanced custom application development <span class="orange-txt">&bull;</span> Researched and implemented new web development technologies and techniques and shared them with the development team <span class="orange-txt">&bull;</span> Mentored junior team members and trained new hires</p>
                            </article>
                            <article>
                            	<p><strong>Owner</strong> / VAMP Website Management System</p>
                                <p><span>March 2011 - Present</span></p>
                                <p>Provide clients with the ability to control every aspect of their website including building templates, managing web pages, navigation links, web assets, custom layouts and content areas, system users, etc <span class="orange-txt">&bull;</span> Developed entire system from scratch using HTML, CSS, jQuery, AJAX, PHP, and MySQL <span class="orange-txt">&bull;</span> Supports responsive design and development <span class="orange-txt">&bull;</span> Release updates as features are added and bugs/issues are fixed</p>
                            </article>
                        </div>
                        <div class="bk-profile right">
                        	<h2>Some <span>Skills</span></h2>
                            <article>
                            	<div class="bk-skill-label html clear">
                                	HTML
                                    <span>100%</span>
                                </div>
                                <div class="bk-skill-bar load html"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label css clear">
                                	CSS
                                    <span>100%</span>
                                </div>
                                <div class="bk-skill-bar load css"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label responsive clear">
                                	Responsive
                                    <span>100%</span>
                                </div>
                                <div class="bk-skill-bar load responsive"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label standards clear">
                                	Web Standards/Optimization
                                    <span>100%</span>
                                </div>
                                <div class="bk-skill-bar load standards"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label cms clear">
                                	CMS
                                    <span>100%</span>
                                </div>
                                <div class="bk-skill-bar load cms"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label javascript clear">
                                	JavaScript/jQuery
                                    <span>95%</span>
                                </div>
                                <div class="bk-skill-bar load javascript"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label adobe clear">
                                	Photoshop/Illustrator/Fireworks
                                    <span>85%</span>
                                </div>
                                <div class="bk-skill-bar load adobe"></div>
                            </article>
							<article>
                            	<div class="bk-skill-label ally clear">
                                	Accessibility
                                    <span>75%</span>
                                </div>
                                <div class="bk-skill-bar load ally"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label php clear">
                                	PHP
                                    <span>70%</span>
                                </div>
                                <div class="bk-skill-bar load php"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label angular clear">
                                	Angular JS
                                    <span>70%</span>
                                </div>
                                <div class="bk-skill-bar load angular"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label mysql clear">
                                	MySQL
                                    <span>65%</span>
                                </div>
                                <div class="bk-skill-bar load mysql"></div>
                            </article>
							<article>
                            	<div class="bk-skill-label git clear">
                                	git
                                    <span>60%</span>
                                </div>
                                <div class="bk-skill-bar load git"></div>
                            </article>
                            <article>
                            	<div class="bk-skill-label gulp clear">
                                	gulp
                                    <span>50%</span>
                                </div>
                                <div class="bk-skill-bar load gulp"></div>
                            </article>
                        </div>
                    </div>
                </section>
            </section>
            <section id="bk-bio-outer" class="bk-section-parent border-box">
            	<div class="bk-mobile-bg bio"></div>
                <div id="bk-bio" class="bk-section clear">
                	<div class="bk-bio right">
                        <h3>This Is <span>Me</span></h3>
                        <h2>Web <span>Developer</span></h2>
                        <p>I've been developing websites since 2009 and I can't imagine myself in any other profession. The ever changing nature of the field is what makes it so much fun. I love learning new things in web development and it's always a great feeling when you build something brand new or solve a complex problem and you feel like you've moved up a skill level. Working on web projects with friends is something I also enjoy. It's a great opportunity to learn from each others strengths and it's also just a blast!</p>
                        <p>Front-end web development is my passion. I also have experience with server-side programming but as the field moves forward, there is more and more of a demand for front-end solutions backed with API's and other web services. Building a dynamic, responsive, accessible, and cross-browser web solution can be a real challenge. Having the skill set to do this is important and valuable in today's web development field.</p>
                        <p>When I get the chance to get away from the computer, I enjoy mountain biking, snowboarding, camping, traveling, and pistol shooting. I was recently fortunate enough to take a trip out and hike the Grand Canyon. That is an experience that I recommend to everyone! It's great to have those opportunities where you can turn off the computer and the cell phone and just let your day-to-day life go!</p>
                        <p>As far as my education is concerned, I graduated from Pennsylvania College of Technology in 2006 with an Associate of Applied Arts degree in Mass Media Communications. After working in the video production field for a while, I decided to go back for more. In 2010, I graduated from Indiana University of Pennsylvania with a Bachelor of Science degree in Communications Media. Near the end of my Bachelor degree, I was offered a full Graduate Assistantship from the college of Education and Educational Technology where I then decided to stay another year to receive a Master of Arts degree in Adult Education and Communications Technology.</p>
                    </div>
                </div>
            </section>
            <section id="bk-contact-outer" class="bk-section-parent border-box">
            	<div id="bk-contact" class="bk-section">
                	<h2>Contact <span>Me</span></h2>
                    <form id="bk-contact-form">
                    	<label for="bk-form-name" class="accessible-hidden">Your Name</label>
                    	<input type="text" id="bk-form-name" class="bk-form-input name border-box" placeholder="Your Name" />
                        <label for="bk-form-email" class="accessible-hidden">Your Email</label>
                        <input type="email" id="bk-form-email" class="bk-form-input email border-box" placeholder="Your Email" />
                        <label for="bk-form-subject" class="accessible-hidden">Your Subject</label>
                        <input type="text" id="bk-form-subject" class="bk-form-input subject border-box" placeholder="Your Subject" />
                        <label for="bk-form-message" class="accessible-hidden">Your Message</label>
                        <textarea id="bk-form-message" class="bk-form-input message border-box" placeholder="Your Message"></textarea>
                        <input type="submit" class="bk-form-input send border-box" value="SHOOT MESSAGE" />
                    </form>
                </div>
            </section>
        </main>
        <footer>
        	<section id="bk-footer-outer" class="bk-section-parent border-box">
            	<div id="bk-footer" class="bk-section">
                	<p>Design by TemplateMO</p>
                </div>
            </section>
        </footer>
    </div>
</body>
</html>